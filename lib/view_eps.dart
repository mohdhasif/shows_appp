// import 'package:flutter/material.dart';
// import 'homeSearch.dart';
// import 'post_model.dart';

// class ViewEps extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         // appBar: AppBar(
//         //   title: new Image.asset('lib/images/newLogo.png', height: 20.0),
//         //   backgroundColor: Colors.grey[50],
//         //   elevation: 10.0,

//         //   // Add new icon
//         //   actions: [
//         //     new IconButton(
//         //       icon: new Icon(Icons.search),
//         //       onPressed: () {},
//         //     ),
//         //   ],

//         //   // Change style of icon
//         //   iconTheme: IconThemeData(
//         //     color: Colors.grey,
//         //   ),

//         //   textTheme: TextTheme(
//         //     headline6: TextStyle(color: Colors.black, fontSize: 20.0),
//         //   ),
//         // ),
//         body: ViewSearch());
//   }
// }

// class ViewSearch extends StatefulWidget {
//   @override
//   _ViewSearchState createState() => _ViewSearchState();
// }

// class _ViewSearchState extends State<ViewSearch> {
//   // ignore: deprecated_member_use
//   List<Post> _posts = List<Post>();
//   // ignore: deprecated_member_use
//   List<Post> _postsDisplay = List<Post>();

//   @override
//   void initState() {
//     fetchPost().then((value) {
//       print(value);
//       setState(() {
//         _posts.addAll(value);
//         _postsDisplay = _posts;
//       });
//     });
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Shows list'),
//       ),
//       body: ListView.builder(
//         itemBuilder: (context, index) {
//           if (_postsDisplay.length > 0) {
//             return index == 0 ? _searchBar() : _listItem(index - 1);
//           } else {
//             return Center(child: CircularProgressIndicator());
//           }
//         },
//         itemCount: _postsDisplay.length + 1,
//       ),
//     );
//   }

//   _searchBar() {
//     return Padding(
//       padding: EdgeInsets.all(8),
//       child: TextField(
//         decoration: InputDecoration(hintText: 'Search...'),
//         onChanged: (text) {
//           text = text.toLowerCase();
//           setState(() {
//             _postsDisplay = _posts.where((post) {
//               var postTitle = post.show.genres;
//               return postTitle.contains(text);
//             });
//           });
//         },
//       ),
//     );
//   }

//   _listItem(index) {
//     return Card(
//       child: Padding(
//         padding: EdgeInsets.only(top: 32, bottom: 32, left: 16, right: 16),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Text(
//               '${_postsDisplay[index].show}',
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// ///////////////////////////////////////////////////////////////////
//
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Api Filter list Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new ExamplePage(),
    );
  }
}

class ExamplePage extends StatefulWidget {
  // ExamplePage({ Key key }) : super(key: key);
  @override
  _ExamplePageState createState() => new _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage> {
  // final formKey = new GlobalKey<FormState>();
  // final key = new GlobalKey<ScaffoldState>();
  final TextEditingController _filter = new TextEditingController();
  // final dio = new Dio();
  String _searchText = "";
  List names = new List();
  List filteredNames = new List();
  List postData = new List();
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = new Text('Search Example');

  _ExamplePageState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredNames = names;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  @override
  void initState() {
    this._getNames();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: Container(
        child: _buildList(),
      ),
      resizeToAvoidBottomInset: false,
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: _appBarTitle,
      leading: new IconButton(
        icon: _searchIcon,
        onPressed: _searchPressed,
      ),
    );
  }

  Widget _buildList() {
    print('${postData[0]["name"]}');
    if (!(_searchText.isEmpty)) {
      List tempList = new List();
      for (int i = 0; i < filteredNames.length; i++) {
        if (filteredNames[i]['name']
            .toLowerCase()
            .contains(_searchText.toLowerCase())) {
          tempList.add(filteredNames[i]);
        }
      }
      filteredNames = tempList;
    }
    return ListView.builder(
      itemCount: postData == null ? 0 : postData.length,
      itemBuilder: (BuildContext context, int index) {
        return new ListTile(
          title: Text('${postData[index]["name"]}'),
          onTap: () => print('${postData[index]["name"]}'),
        );
      },
    );
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search), hintText: 'Search...'),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = new Text('Search Example');
        filteredNames = names;
        _filter.clear();
      }
    });
  }

  void _getNames() async {
    List postData;
    List tempList = new List();
    String name;
    String id;
    var url =
        // Uri.https('www.googleapis.com', '/books/v1/volumes', {'q': '{http}'});
        Uri.https('api.tvmaze.com', 'shows', {'q': 'man'});

    print(url);
    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);
    if (response.statusCode == 200) {
      // data = convert.jsonDecode(response.body);
      var jsonResponse = convert.jsonDecode(response.body);
      setState(() {
        postData = jsonResponse;
      });
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }
}

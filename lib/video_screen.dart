import 'package:flutter/material.dart';
// import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoScreen extends StatefulWidget {
  // String id;
  String id;
  String name;
  String uri;
  String season;
  String number;
  String type;
  String airdate;
  String runtime;
  String medium;
  String original;
  String summary;

  VideoScreen(
      {this.id,
      this.name,
      this.uri,
      this.season,
      this.number,
      this.type,
      this.airdate,
      this.medium,
      this.original,
      this.summary,
      this.runtime});

  @override
  _VideoScreenState createState() => _VideoScreenState(id, name, uri, season,
      number, type, airdate, medium, original, runtime, summary);
}

class _VideoScreenState extends State<VideoScreen> {
  // YoutubePlayerController _controller;

  String id;
  String name;
  String uri;
  String season;
  String number;
  String type;
  String airdate;
  String runtime;
  String medium;
  String original;
  String summary;

  _VideoScreenState(id, name, uri, season, number, type, airdate, medium,
      original, runtime, summary);

  @override
  void initState() {
    super.initState();
    // _controller = YoutubePlayerController(
    //   initialVideoId: widget.id,
    //   flags: YoutubePlayerFlags(
    //     mute: false,
    //     autoPlay: true,
    //   ),
    // );
  }

  @override
  Widget build(BuildContext context) {
    print(widget.summary);
    // return Scaffold(
    //   appBar: AppBar(
    //     title: Text('Shows Details'),
    //     backgroundColor: Colors.blue,
    //   ),
    //   body: FadeInImage(
    //     image: NetworkImage(widget.original),
    //     placeholder: NetworkImage(widget.original),
    //   ),
    // );
    return Scaffold(
        appBar: AppBar(
          title: Text('Shows Details'),
          backgroundColor: Colors.blue,
        ),
        body: Column(children: [
          Expanded(
            child: Column(
              children: [
                Container(
                  height: 250.0,
                  child: new Column(
                    children: new List.generate(
                        1,
                        (index) => new SecondBody(
                            widget.id,
                            widget.name,
                            widget.uri,
                            widget.season,
                            widget.number,
                            widget.type,
                            widget.airdate,
                            widget.medium,
                            widget.original,
                            widget.runtime,
                            widget.summary)),
                  ),
                ),
                Expanded(
                  child: ListView(
                    children: [
                      Container(
                        child: new VideoDetails(
                            widget.id,
                            widget.name,
                            widget.uri,
                            widget.season,
                            widget.number,
                            widget.type,
                            widget.airdate,
                            widget.medium,
                            widget.original,
                            widget.runtime,
                            widget.summary),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ])

        //dont touch
        );
  }
}

class SecondBody extends StatelessWidget {
  String id;
  String name;
  String uri;
  String season;
  String number;
  String type;
  String airdate;
  String runtime;
  String medium;
  String original;
  String summary;
  SecondBody(this.id, this.name, this.uri, this.season, this.number, this.type,
      this.airdate, this.medium, this.original, this.runtime, this.summary);

  @override
  Widget build(BuildContext context) {
    // Video preview image
    return new Column(
      children: [
        Container(
          // width: screenWidth,
          height: 250.0,
          margin: const EdgeInsets.all(0.0),
          decoration: new BoxDecoration(
            image: DecorationImage(
              image: new NetworkImage(this.original),
              fit: BoxFit.fill,
            ),
          ),
        ), //HERE ADD
      ],
    );
  }
}

class VideoDetails extends StatelessWidget {
  String id;
  String name;
  String uri;
  String season;
  String number;
  String type;
  String airdate;
  String runtime;
  String medium;
  String original;
  String summary;
  VideoDetails(
      this.id,
      this.name,
      this.uri,
      this.season,
      this.number,
      this.type,
      this.airdate,
      this.medium,
      this.original,
      this.runtime,
      this.summary);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(
        children: <Widget>[
          Column(children: <Widget>[
            Container(
              alignment: Alignment.topLeft,
              child: new Text(
                this.name,
                style: TextStyle(fontSize: 16.0, color: Colors.black),
              ),
              // width: screenWidth - 80.0,
              margin: EdgeInsets.only(left: 15.0, bottom: 5.0),
            ),
            Container(
              // width: screenWidth - 80.0,
              margin: EdgeInsets.only(left: 15.0),
            ),
          ]),
          Container(
            child: new IconButton(
              icon: new Icon(
                Icons.arrow_drop_down,
                color: Colors.grey[700],
              ),
              onPressed: () {},
            ),
            width: 50.0,
            height: 30.0,
            margin: EdgeInsets.only(right: 15.0, bottom: 5.0),
          ),
        ],
      ),
      Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: <Widget>[
        Column(children: <Widget>[
          Container(
            child: new Text(
                'Description: ${this.summary.replaceAll("<p>", "").replaceAll("</p>", "")}'),
            width: 300.0,
          ),
        ]),
      ]),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          //FIRST COLUMN
          Column(children: <Widget>[
            Container(
              child: new IconButton(
                icon: new Icon(Icons.thumb_up, color: Colors.grey[600]), // Like
                onPressed: () {},
              ),
              margin: EdgeInsets.only(left: 0.0),
            ),
            Container(
              child: new Text('dynamic code'),
            ),
          ]),
          Column(children: <Widget>[
            Container(
              child: new IconButton(
                icon:
                    new Icon(Icons.thumb_down, color: Colors.grey[600]), // Like
                onPressed: () {},
              ),
              margin: EdgeInsets.only(left: 0.0),
            ),
            Container(
              child: new Text('hard code'),
            ),
          ]),
          //SECOND COLUMN
          Column(children: <Widget>[
            Container(
              child: new IconButton(
                icon: new Icon(Icons.share, color: Colors.grey[600]), // Share
                onPressed: () {},
              ),
            ),
            Container(
              child: new Text('Share'),
            ),
          ]),
          //FIFTH COLUMN
        ],
      ),
    ]);
  }
}

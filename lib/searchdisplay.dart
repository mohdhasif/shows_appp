import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:shows_appp/video_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
//Step 3
  _HomeScreenState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredNames = names;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

//Step 1
  final TextEditingController _filter = new TextEditingController();
  final dio = new Dio(); // for http requests
  String _searchText = "";
  List names = new List(); // names we get from API
  List filteredNames = new List(); // names filtered by search text
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = new Text('Search Example');
  List userData;
  int number;

  //step 2.1
  void _getNames() async {
    var url =
        // Uri.https('www.googleapis.com', '/books/v1/volumes', {'q': '{http}'});
        Uri.https('api.tvmaze.com', 'seasons/1/episodes');

    // final response = await dio.get('https://api.tvmaze.com/seasons/1/episodes');
    // var response = await http.get(url);
    // print(url);
    // print(response.body);
    // print(response.data);
    List tempList = new List();
    var response = await http.get(url);

    var jsonResponse = convert.jsonDecode(response.body);

    // print('${jsonResponse[0]["name"]}');
    // setState(() {
    //   userData = jsonResponse;
    // });

    for (int i = 0; i < jsonResponse.length; i++) {
      tempList.add(jsonResponse[i]);
    }

    // print(tempList);

    // for (int i = 0; i < jsonResponse.length; i++) {
    //   tempList.add(jsonResponse[i]["name"]);
    // }
    // print(tempList);
    setState(() {
      // userData = jsonResponse;
      names = tempList;
      filteredNames = names;
    });
  }

//Step 2.2
  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search), hintText: 'Search...'),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = new Text('Search Example');
        filteredNames = names;
        _filter.clear();
      }
    });
  }

  //Step 4
  Widget _buildList() {
    if (!(_searchText.isEmpty)) {
      List tempList = new List();
      for (int i = 0; i < filteredNames.length; i++) {
        if (filteredNames[i]['name']
            .toLowerCase()
            .contains(_searchText.toLowerCase())) {
          // print(userData[i]['name']);
          // print(_searchText);
          tempList.add(filteredNames[i]);
        }
      }
      print(tempList);
      filteredNames = tempList;
    }
    return ListView.builder(
      itemCount: names == null ? 0 : filteredNames.length,
      itemBuilder: (BuildContext context, int index) {
        // return new ListTile(
        //   title: Text(filteredNames[index]['name']),
        //   onTap: () => print(filteredNames[index]['name']),
        // );
        if (filteredNames == '${filteredNames[index]["name"]}') {
          return GestureDetector(
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => VideoScreen(
                  id: '${filteredNames[index]["id"]}',
                  name: '${filteredNames[index]["name"]}',
                  uri: '${filteredNames[index]["url"]}',
                  season: '${filteredNames[index]["season"]}',
                  number: '${filteredNames[index]["number"]}',
                  type: '${filteredNames[index]["type"]}',
                  airdate: '${filteredNames[index]["airdate"]}',
                  runtime: '${filteredNames[index]["runtime"]}',
                  medium: '${filteredNames[index]["image"]["medium"]}',
                  original: '${filteredNames[index]["image"]["original"]}',
                  summary: '${filteredNames[index]["summary"]}',
                ),
              ),
            ),
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
              padding: EdgeInsets.all(10.0),
              height: 140.0,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    offset: Offset(0, 1),
                    blurRadius: 6.0,
                  ),
                ],
              ),
              child: Row(
                children: <Widget>[
                  Image(
                    width: 150.0,
                    image: NetworkImage(
                        '${filteredNames[index]["image"]["original"]}'),
                  ),
                  SizedBox(width: 10.0),
                  Expanded(
                    child: Text(
                      '${filteredNames[index]["name"]}',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        } else {
          return GestureDetector(
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => VideoScreen(
                  id: '${filteredNames[index]["id"]}',
                  name: '${filteredNames[index]["name"]}',
                  uri: '${filteredNames[index]["url"]}',
                  season: '${filteredNames[index]["season"]}',
                  number: '${filteredNames[index]["number"]}',
                  type: '${filteredNames[index]["type"]}',
                  airdate: '${filteredNames[index]["airdate"]}',
                  runtime: '${filteredNames[index]["runtime"]}',
                  medium: '${filteredNames[index]["image"]["medium"]}',
                  original: '${filteredNames[index]["image"]["original"]}',
                  summary: '${filteredNames[index]["summary"]}',
                ),
              ),
            ),
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
              padding: EdgeInsets.all(10.0),
              height: 140.0,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    offset: Offset(0, 1),
                    blurRadius: 6.0,
                  ),
                ],
              ),
              child: Row(
                children: <Widget>[
                  Image(
                    width: 150.0,
                    image: NetworkImage(
                        '${filteredNames[index]["image"]["original"]}'),
                  ),
                  SizedBox(width: 10.0),
                  Expanded(
                    child: Text(
                      '${filteredNames[index]["name"]}',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        }
        ;
      },
    );
  }

  //STep6
  Widget _buildBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: _appBarTitle,
      leading: new IconButton(
        icon: _searchIcon,
        onPressed: _searchPressed,
      ),
    );
  }

  @override
  void initState() {
    _getNames();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: Container(
        child: _buildList(),
      ),
//      floatingActionButton: FloatingActionButton(
//        onPressed: _postName,
//        child: Icon(Icons.add),
//      ),
    );
  }
}

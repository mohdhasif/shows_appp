// import 'dart:developer';

import 'package:flutter/material.dart';
// import 'package:http/http.dart';
import 'package:http/http.dart' as http;
// import 'dart:async';
import 'dart:convert' as convert;
import 'package:shows_appp/video_screen.dart';

void main() {
  runApp(MaterialApp(
    home: JsonDemo(),
  ));
}

class JsonDemo extends StatefulWidget {
  @override
  _JsonDemoState createState() => _JsonDemoState();
}

// http://api.tvmaze.com/seasons/1/episodes
class _JsonDemoState extends State<JsonDemo> {
  Map data;
  List userData;
  String videoPreview;
  void getJsonData() async {
    var url =
        // Uri.https('www.googleapis.com', '/books/v1/volumes', {'q': '{http}'});
        Uri.https('api.tvmaze.com', 'seasons/1/episodes');

    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);
    if (response.statusCode == 200) {
      // data = convert.jsonDecode(response.body);
      var jsonResponse = convert.jsonDecode(response.body);
      setState(() {
        userData = jsonResponse;
      });
      // var itemCount = jsonResponse['totalItems'];

      // print(userData.toString());
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Testing TV MAZE'),
        backgroundColor: Colors.blue,
      ),
      body: ListView.builder(
        itemCount: userData == null ? 0 : userData.length,
        itemBuilder: (BuildContext context, int index) {
          return _buildVideo(index);
          // return Card(
          //   child: Row(
          //     children: <Widget>[Text('${userData[index]["id"]}')],
          //   ),
          // );
          // return FadeInImage(
          //   image: NetworkImage('${userData[index]["image"]["original"]}'),
          //   placeholder:
          //       NetworkImage('${userData[index]["image"]["original"]}'),
          // );
          // return Padding(
          //   padding: EdgeInsets.all(20),
          //   child: Column(
          //     children: <Widget>[
          //       Image.network('${userData[index]["image"]["original"]}'),
          //       Text('${userData[index]["name"]}')
          //     ],
          //   ),
          // );
        },
      ),
    );
  }

  _buildVideo(num) {
    print('${userData[num]["image"]["original"]}');
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => VideoScreen(
            id: '${userData[num]["id"]}',
            name: '${userData[num]["name"]}',
            uri: '${userData[num]["url"]}',
            season: '${userData[num]["season"]}',
            number: '${userData[num]["number"]}',
            type: '${userData[num]["type"]}',
            airdate: '${userData[num]["airdate"]}',
            runtime: '${userData[num]["runtime"]}',
            medium: '${userData[num]["image"]["medium"]}',
            original: '${userData[num]["image"]["original"]}',
            summary: '${userData[num]["summary"]}',
          ),
        ),
      ),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
        padding: EdgeInsets.all(10.0),
        height: 140.0,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              offset: Offset(0, 1),
              blurRadius: 6.0,
            ),
          ],
        ),
        child: Row(
          children: <Widget>[
            Image(
              width: 150.0,
              image: NetworkImage('${userData[num]["image"]["original"]}'),
            ),
            SizedBox(width: 10.0),
            Expanded(
              child: Text(
                '${userData[num]["name"]}',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.0,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
